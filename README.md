# CapSo frontend developer test task #

## Getting started ##

* Use bower and npm to install dependencies
* Run using `grunt serve`
* Go to localhost:9000 and further instructions will be provided there.

## Notes ##

* Good UI (Tried my best);
* Friendly URL ex: company/1/coca-cola-inc;
* Filter (GroupBy) in Bond history page;
* Option to enter the start and end date to see the bonds;
* Highest five moving bond prices by date range;